<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/
$factory->define(App\Models\Like::class, function (Faker\Generator $faker) {
    return [
        'phoneId' => $faker->numberBetween(1, 500000),
        'value' => $faker->randomElement($array = array ('-1','1')),
        'ip' => $faker->ipv4,
        'agent' => $faker->userAgent,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        'time'=>$faker->unixTime,
    ];
});

$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
      'name' => $faker->name,
      'email' => $faker->unique()->safeEmail,
      'password' => $password ?: $password = bcrypt('secret'),
      'remember_token' => str_random(10),
    ];
});

$factory->define(App\Models\Phone::class, function (Faker\Generator $faker) {
    $phone = explode('x', $faker->phoneNumber) [0];

    return [
      'number' => $phone,
      'prefix' => $faker->numberBetween(0, 10),
      'country' => 'AD',
      'aliases' => "+$phone|tel:+$phone",
      'url' => '',
      'page' => $faker->numberBetween(1, 1000),
    ];
});
$factory->define(App\Models\View::class, function (Faker\Generator $faker) {

    return [
        'phoneId' => $faker->numberBetween(1, 500000),
        'count' => $faker->numberBetween(1, 1000),
        'ip' => $faker->ipv4,
        'agent' => $faker->userAgent,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
    ];
});


