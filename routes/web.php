<?php
Route::get('/login', function () {
    return view('pages.login');
});
//Route::get('/register', function () {
//    return view('pages.register');
//});
Route::post('login', 'Auth\LoginController@login');
Route::get('logout', 'Auth\LoginController@logout');
//Route::post('register', 'Auth\RegisterController@register');
Route::group(['middleware' => 'auth'], function () {
    Route::get('/admin', 'AdminController@index');
    Route::get('/admin/report', 'AdminController@report');
    Route::get('/admin/settings', 'AdminController@showSettings');
    Route::get('/admin/settings/edit/{id}', 'AdminController@editSettings');
    Route::patch('admin/settings/{id}', 'AdminController@updateSettings');
    Route::get('/admin/delete', 'AdminController@showDelete');
    Route::post('/admin/delete/{id}', 'AdminController@deleteNumber');
    Route::delete('/admin/report/{id}', 'AdminController@destroyReport');
    Route::get('/admin/phone', 'AdminController@create');
    Route::post('/admin/phone', 'AdminController@store');
    Route::get('/admin/views', 'AdminController@viewsDiagram');
    Route::get('/admin/translation', 'AdminController@indexTranslate');
    Route::get('/admin/translation/{name}', 'AdminController@showTranslate');
    Route::patch('/admin/translation/{name}', 'AdminController@updateTranslate');
    Route::get('/blog/create', 'ArticleController@create');
    Route::post('/blog/create', 'ArticleController@store');
    Route::get('admin/blog/edit/{id}', 'ArticleController@edit');
    Route::patch('/blog/edit/{id}', 'ArticleController@update');
    Route::delete('/blog/{id}', 'ArticleController@destroy');
    Route::resource('admin/area', 'AreaController', ['except' => ['show']]);
    Route::get('admin/safe-unsafe', 'IpController@fakeSafeUnsafe');
    Route::resource('admin/ips', 'IpController', ['except' => ['show']]);

});

Route::group(['middleware' => 'black.list'], function () {
    Route::get('/', 'PagesController@home');
    Route::get('/blog', 'ArticleController@index');
    Route::get('/blog/{id}', 'ArticleController@show');
    Route::post('/blog/{id}/comment', 'ArticleController@storeComment');
//    Route::get('/phones', 'PagesController@phones');
    Route::get('/phone/{number}', 'PagesController@phone');
    Route::get('/report/{number?}', 'PagesController@report');
    Route::get('/about', 'PagesController@about');
//    Route::get('/faq', 'PagesController@faq');
    Route::get('/prefix', 'AreaController@areaCode');
    Route::get('/prefix/{prefix}', 'AreaController@areaCodeNumbers');
    Route::get('/prefix/{prefix}/code/{code}', 'AreaController@suitedNumbers');
    Route::get('/contact_us', 'PagesController@getContacts');
    Route::get('/top-safe', 'PagesController@getSafe');
    Route::get('/top-unsafe', 'PagesController@getUnsafe');
    Route::get('/privacy_policy', 'PagesController@getPrivacy');
    Route::get('/terms_of_use', 'PagesController@getTerms');
    Route::get('phone/detailed/{phone}', 'PagesController@details');
    Route::get('/get_captcha/{config?}', function (\Mews\Captcha\Captcha $captcha, $config = 'default') {
        return $captcha->src($config);
    });


//    Route::get('countries', 'PagesController@countries');
    Route::resource('top-search', 'SearchController');


    Route::post('/phone/{number}/like', 'ActionsController@like');
    Route::post('/phone/{number}/comment', 'ActionsController@comment');
    Route::post('/phone/report', 'ActionsController@report');
    Route::post('/admin_message', 'PagesController@admin_message');
});
