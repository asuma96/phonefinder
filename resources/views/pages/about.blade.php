@extends('layouts.default')
@section('content')

    <div>
        {{ Html::link('/', trans('pages.links.home'))}}
        / {{trans('pages.links.about')}}
    </div>

    <h3>{{trans('pages.about.1_line')}}</h3>
    <hr>

    <p>{{trans('pages.about.2_line')}}</p>
    <p>{{trans('pages.about.3_line')}}</p>
@stop