@extends('layouts.default')
@section('content')
    <div class="container">
        <div>
            {{ Html::link('/', trans('pages.links.home'))}}
            / {{ Html::link('/prefix', trans('area.prefix'))}}
            / {{$prefix}}
            {{--/ {{ Html::link('/prefix/' . $prefix, $prefix)}}--}}
        </div>
        <h2>{{trans('area.area')}} {{$prefix}}  {{trans('area.city')}}
            : @if(isset($city->location)){{$city->location}}@else  @endif</h2>
        <table class="table">
            <thead>
            <tr>
                <th>{{trans('area.prefix')}}</th>
                <th>{{trans('area.link')}}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($full_prefix as $item)
                <tr>
                    <td>{{$item->area_number}}</td>
                    <td>{{ Html::link('/prefix/' . $item->prefix . '/code/' . $item->area_number,trans('area.browse') . $item->prefix . '-' . $item->area_number . '-ABCD')}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop