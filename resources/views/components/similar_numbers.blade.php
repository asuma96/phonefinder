<table class="table">
    <tr>
        <td>{{trans('phone.number')}}</td>
        <td>{{trans('phone.prefix')}}</td>
        <td>{{trans('phone.similar_numbers')}}</td>
    </tr>
    @foreach($data as $item)
        <tr>
            <td>
                {{ Html::link('/phone/' . $item->short_number, $item->short_number)}}
            </td>
            <td>
                {{$item->prefix}}
            </td>
            <td>
                {{$item->aliases}}
            </td>
        </tr>
    @endforeach
</table>