{{--<div class="col-lg-12">--}}


<div class="footer-bottom">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-s col-md-10 col-lg-10">
                <span class="copyright footer-link">{{trans('pages.copyright')}} &copy; {{trans('pages.title_site')}}
                    2017</span>
                <span class="footer-link">{{ Html::link('/about', trans('pages.links.about'))}}</span>
                <span class="footer-link">{{ Html::link('/contact_us', trans('pages.links.contact_us'))}}</span>
                <span class="footer-link">{{ Html::link('/privacy_policy', trans('pages.links.privacy_policy'))}}</span>
                <span class="footer-link">{{ Html::link('/terms_of_use', trans('pages.links.terms_of_use'))}}</span>
            </div>
        </div>
    </div>
</div>
{{--</div>--}}