<?php

namespace App\Console\Commands;

use App\Helpers\DataCacheHelper;
use App\Models\Ip;
use App\Models\Like;
use App\Models\Phone;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class FakeSafeUnsafe extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:fake_safe_unsafe';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $count = (DataCacheHelper::redis('prefix_count', '', 'addYears', '2', function () {
            return Phone::query()->count();
        }));
        DB::select('drop table if EXISTS test');
        DB::select('create table fake_like like phone');
        DB::select('insert into fake_like (id,number)
              select id,number from phone where id not in
              (select phoneId from likes group by phoneId)'
        );
        $offset = 0;
        $likes = (($count - $count%10) - count(Like::query()->groupBy('phoneId')->get()));
        $ips = Ip::query()->get();
        $time_start = time();
        if (count($ips) > 0) {
            while($offset < $likes){
                $phones = DB::table('temp_table')->take(10)->get();
                $this->createFakeSafeUnsafe($phones, $ips);
                foreach ($phones as $phone) {
                    DB::table('temp_table')->where('id', $phone->id)->delete();
                }
                $offset +=10;
            }
            $time_end = time();
            DB::select('drop table fake_like');
            $this->info('Done.');
            $this->info(gmdate("H:i:s", $time_end - $time_start));
        } else {
            $this->info('No data for command');
        }
    }

    public function generateIp($ips)
    {
        $ip = $ips->random();
        $ex = explode(".", $ip->value);

        return $ex[0].'.'.$ex[1].'.'.rand(1,255).'.'.rand(1,255);
    }

    public function createFakeSafeUnsafe($phones, $ips)
    {
        $phone = $phones->random(5);
        $i = 0;
        $data = [];
        foreach ($phone as $value) {
            if ($i < 4) {
                array_push($data, $this->createFakeLike($value->id, -1, $ips));

            }else {
                array_push($data, $this->createFakeLike($value->id, 1, $ips));
            }
            $i++;
        }
        Like::insert($data);
    }

    public function createFakeLike($phone_id, $value, $ips)
    {
        $ip = $this->generateIp($ips);
        $u_agent = \Campo\UserAgent::random([
            'os_type' => ['Android', 'iOS', 'Xbox', 'Linux', 'OS X'],
            'device_type' => ['Mobile', 'Tablet', 'Desktop', 'Console']
        ]);
        $time = date("Y-m-d H:i:s", rand(strtotime('2017-02-01 00:00:00'), time()));
        $data = [
            'phoneId'    => $phone_id,
            'value'      => $value,
            'ip'         => $ip,
            'agent'      => $u_agent,
            'created_at' => $time,
            'updated_at' => $time,
        ];

        return $data;
    }
}
