<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BotVisit extends Model
{
    protected $table = 'bot_visits';
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'agent',
      'count'
    ];

}
