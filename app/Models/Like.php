<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    protected $table = 'likes';
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'phoneId',
      'value',
      'ip',
      'agent',
      'created_at',
      'updated_at'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
      'id',
      'ip',
      'agent'
    ];

    //hasOne

    //hasMany

    //belongsTo

    public function phone()
    {
        return $this->belongsTo(Phone::class, 'phoneId', 'id');
    }

    //belongsToMany

    /**
     * VALIDATION RULES
     */
    protected function rules()
    {
        return array();
    }
}
