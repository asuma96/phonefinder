<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $table = 'reports';
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'phoneId',
      'name',
      'email',
      'owner',
      'type',
      'message',
      'rating',
      'ip',
      'agent'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
      'id',
      'ip',
      'agent'
    ];

    /**
     * Default values for attributes
     * @var array an array with attribute as key and default as value
     */
    protected $attributes = [
      'rating' => 0
    ];

    //hasOne

    //hasMany

    //belongsTo

    public function phone()
    {
        return $this->belongsTo(Phone::class, 'phoneId', 'id');
    }

    //belongsToMany

    /**
     * VALIDATION RULES
     */
    protected function rules()
    {
        return array();
    }
}
