<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class View extends Model
{
    protected $table = 'views';
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'phoneId',
      'count',
      'ip',
      'agent',
      'views_count',
      'created_at'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
      'id',
      'agent',
      'updated_at',

    ];

    //hasOne

    //hasMany

    //belongsTo

    public function phone()
    {
        return $this->belongsTo(Phone::class, 'phoneId', 'id');
    }

    //belongsToMany

    /**
     * VALIDATION RULES
     */
    protected function rules()
    {
        return array();
    }
}
