<?php

namespace App\Http\Controllers;

use App\Helpers\DataCacheHelper;
use App\Models\Phone;
use Carbon\Carbon;

class SitemapController extends Controller
{
    public static function index()
    {
        $url = env('APP_URL');
        $base_name_file = 'sitemap';
        $folder = 'sitemap/';
        $base_url = ['/', '/about/', '/report/', '/top-search/'];
        $limit = 50000;
        $offset_count = 0;
        $count = DataCacheHelper::countRowPhone();
        $query_count = ceil($count / $limit);

        for ($i = 0; $i < $query_count; $i++) {
            $posts = Phone::query()->select('short_number')->skip($offset_count)->limit($limit)->get()->toArray();

            $links = array_map(function ($item) use ($url) {
                return '/phone/' . $item['short_number'];
            }, $posts);

            $offset_count += $limit;
            $name_file = '/' . $folder . $base_name_file . '-' . $i . '.xml';
            array_push($base_url, $name_file);

            self::generateSiteMapXML($url, $links, 'child', $name_file);
        }
        self::generateSiteMapXML($url, $base_url, 'main', 'sitemap-all.xml');
    }

    public static function generateSiteMapXML($url, $links, $type, $file_name)
    {
        if ($type === 'main') {
            $xml = new \SimpleXMLElement('<sitemapindex/>');
            $xml->addAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');
            $link = 'sitemap';
        } else {
            $xml = new \SimpleXMLElement('<urlset/>');
            $xml->addAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');
            $xml->addAttribute('xmlns:xhtml', 'http://www.w3.org/1999/xhtml');
            $xml->addAttribute('xmlns:image', 'http://www.google.com/schemas/sitemap-image/1.1');
            $xml->addAttribute('xmlns:video', 'http://www.google.com/schemas/sitemap-video/1.1');
            $link = 'url';
        }

        foreach ($links as $item) {
            $sitemap = $xml->addChild($link);
            $sitemap->addChild('loc', $url . $item);
            $sitemap->addChild('lastmod', Carbon::now()->toDateString());
            $sitemap->addChild('changefreq', 'monthly');
            $sitemap->addChild('priority', '0.5');
        }

        Header('Content-type: text/xml');
        $xml->asXML('public/' . $file_name);
    }
}
